﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TranslatorTool.Models
{
    public class Translation
    {
        public long Id { get; set; }
        public string userInput { get; set; }
        public string translation { get; set; }
    }
}
