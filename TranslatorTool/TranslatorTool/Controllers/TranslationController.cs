﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using TranslatorTool.Context;
using TranslatorTool.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TranslatorTool.Controllers
{
    [Route("api/[controller]")]
    public class TranslationController : Controller
    {
        private readonly TranslationContext _context;

        public TranslationController(TranslationContext context)
        {
            _context = context;

            if (_context.Translations.Count() == 0)
            {
                _context.Translations.Add(new Translation { userInput = "Zdravo", translation = "Hello" });
                _context.SaveChanges();
            }
        }

        // GET: api/<controller>
        [HttpGet]
        public Translation GetAll()
        {
           // return await _context.Translations.ToListAsync();
            Translation translation = _context.Translations.Last();
            return translation;
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public async Task<Translation> Post([FromBody]Translation trans)
        {
            foreach(Translation t in _context.Translations)
            {
                if (trans.userInput.Equals(t.userInput)) {
                    trans.translation = t.translation;
                    return trans;
                }
            }
            

            string host = "https://api.cognitive.microsofttranslator.com";
            string route = "/translate?api-version=3.0&to=en";
            string subscriptionKey = "443b706fbeb9401ab897e14614948db4";
            
            string Text = trans.userInput;
            System.Object[] body = new System.Object[] { new { Text } };
            var requestBody = JsonConvert.SerializeObject(body);
            
            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage())
            {
                // Set the method to POST
                request.Method = HttpMethod.Post;

                // Construct the full URI
                request.RequestUri = new Uri(host + route);

                // Add the serialized JSON object to your request
                request.Content = new StringContent(requestBody, Encoding.UTF8, "application/json");

                // Add the authorization header
                request.Headers.Add("Ocp-Apim-Subscription-Key", subscriptionKey);

                // Send request, get response
                var response = client.SendAsync(request).Result;
                var jsonResponse = response.Content.ReadAsStringAsync().Result;
               
                trans.translation = jsonResponse.ToString();
            }
            _context.Translations.Add(trans);
            await _context.SaveChangesAsync();

            return trans;
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
