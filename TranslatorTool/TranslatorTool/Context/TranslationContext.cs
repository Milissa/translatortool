﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TranslatorTool.Models;

namespace TranslatorTool.Context
{
    public class TranslationContext : DbContext
    {
        public TranslationContext(DbContextOptions<TranslationContext> options) : base(options)
        {
        }

        public DbSet<Translation> Translations { get; set; }
    }
}
