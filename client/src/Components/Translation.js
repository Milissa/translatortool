import React, { Component } from 'react';
import { MDBInput, Button, MDBContainer, MDBRow, MDBCol } from 'mdbreact';
import { Link } from 'react-router-dom'
import axios from 'axios';
import IdleTimer from 'react-idle-timer';
import FinalTranslation from '../Components/FinalTranslation'

class Translation extends Component {
    constructor(props){
        super(props);

        this.state = { 
            userInput: "" ,
            translation: [],
            translationFinal: [],
            
        }
        this.idleTimer = null
    }


    sendInput = (value) => {
        var that = this;
        axios.post('https://localhost:44306/api/translation', {
            userInput: value
          })
          .then(function (response) {
            var array = JSON.parse(response.data.translation);
            that.setState({translation: array[0].translations[0].text})
          })
    }

    redirect = () =>{
        
        window.location = "http://localhost:3000/translation"
    }
   

    onChangeEvent = e => {
        this.setState({
            userInput: e.target.value
        })
        this.sendInput(e.target.value)
    }

    afterSeconds = () => {
        this.setState({
            translationFinal: this.state.translation
        })
    }

    render(){
        return(
            <div>

                <div class="redHeader">
                    <p></p>
                </div>
                
                <div class="blueContainer">
                    <input onChange={this.onChangeEvent} type="text" class="inputBox"/>
                </div>

                
                <div className="left">
                <span class="text">Translated text:</span>
                <span class="custom"> {JSON.stringify(this.state.translationFinal)} </span>
                
                </div>
                  
                <IdleTimer ref={ref => { this.idleTimer = ref }} element={document} onIdle={() => { this.sendInput(); this.afterSeconds() }} debounce={250} timeout={3000} />

                <button className="button right" onClick={ this.redirect}> Ok - continue </button>
            </div>
        )
    }

}

export default Translation;