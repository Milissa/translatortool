import React from 'react';
import { Navbar, NavbarBrand } from 'mdbreact';
import { BrowserRouter as Router} from 'react-router-dom';

class NavbarJS extends React.Component {

    render() {
        return (
            <Router>
                <div>
                <Navbar color="special-color" dark expand="md" scrolling>
                    <div className = "white-text">
                        TranslatorTool
                    </div>
                </Navbar>
                </div>
            </Router>
        );
    }
}

export default NavbarJS;
