import React, { Component } from 'react';
import { MDBInput, Button, MDBContainer, MDBRow, MDBCol } from 'mdbreact';
import axios from 'axios';
import IdleTimer from 'react-idle-timer';

class FinalTranslation extends Component {
    constructor(props){
        super(props);

        this.state = { 
            userInput: "" ,
            translation: [],
            
        }
        this.idleTimer = null
    }

    componentDidMount(){
        this.getTranslation()
    }

    getTranslation = () => {
        var that = this;
        axios.get('https://localhost:44306/api/translation')
          .then(function (response) {
            var array = JSON.parse(response.data.translation);
            that.setState({translation: array[0].translations[0].text})
          })
    }

    render(){
        if( this.state.translation !== []){
            return(
                <div>
    
                    <div class="redHeader">
                        <p></p>
                    </div>
                    
                    <div>
                    <span class="text left">Your final translation is: </span>
                    <span class="custom"> {JSON.stringify(this.state.translation)} </span>
                    </div>
                   
    
                   
                    
                </div>
            )
        }else{ return null;}
        
    }

}

export default FinalTranslation;