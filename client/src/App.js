import React, { Component } from 'react';
import './App.css';
import Translation from './Components/Translation';
import FinalTranslation from './Components/FinalTranslation'
import { BrowserRouter as Router, Route } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <Router>
      <div className="App">

      <Route path="/translate" component={Translation} />
          <Route path="/translation" component={FinalTranslation} />
      </div>
         

      </Router>
    );
  }
}

export default App;
